/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

import java.util.Scanner;

/**
 *
 * @author Love_
 */
public class Lab1 {
    
    public static void main(String[] args) {
        playGame();
    }

    public static void playGame() {
        System.out.println("New game OX");
        char[][] table = new char[3][3];
        Scanner scanner = new Scanner(System.in);
        char Player = 'X';
        boolean gameWin = false;
        boolean gameDraw = false;
        
        for(int row = 0; row < table.length; row++) {
           for(int col = 0; col < table[row].length; col++) {
               table[row][col] = ' ';
           } 
        }
        while (!gameWin && !gameDraw) {
            printTable(table);
            System.out.println("Player " + Player + ", enter: ");
            
            int row = scanner.nextInt();
            int col = scanner.nextInt();
            
            if (table[row][col] == ' '){
                table[row][col] = Player;
                gameWin = Winner(table, Player);
                if(gameWin){
                    System.out.println("Player" + Player + "Winer");
                    break;
                } else {

                Player = (Player == 'X') ? 'O' : 'X';
                }
            } else {
                System.out.println("move error. Try again!!!");
            }
            
            gameDraw = GameDraw(table);
             if (gameDraw) {
            System.out.println("The game is a draw!");
            break;
        }
        }
        printTable(table);
        
        System.out.println("Do you want to play again? (Y/N)");
        String playAgain = scanner.next();
        if (playAgain.equalsIgnoreCase("Y")) {
        main(null);
        } else {
        System.out.println("Thank you for playing!");
        }
    }
    
    public static boolean GameDraw(char[][] table) {
        for (int row = 0; row< table.length; row++) {
            for(int col = 0; col < table[row].length; col++) {
                if (table[row][col] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static boolean Winner(char[][] table, char Player){
        //Check rows
        for (int row = 0;row < table.length;row++){
            if(table[row][0] == Player && table[row][1] == Player && table[row][2] == Player) {
                return true;
            }
        }
        //Check cols
         for (int col = 0;col < table.length;col++){
            if(table[0][col] == Player && table[1][col] == Player && table[2][col] == Player) {
                return true;
            }
         }
         if(table[0][0] == Player && table[1][1] == Player && table[2][2] == Player) {
             return true;
         }
         if(table[0][2] == Player && table[1][1] == Player && table[2][0] == Player) {
             return true;
         }
         return false;
    }
    
    public static void printTable(char[][] table) {
        for(int row = 0;row < table.length; row++) {
            for(int col = 0; col < table[row].length; col++){
                System.out.print(table[row][col] + " | ");
            }
            System.out.println();
        }
    }
}
